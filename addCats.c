///////////////////////////////////////////////////////////////////////////////
/////         University of Hawaii, College of Engineering
///// @brief  Lab 05d - Animal farm 0 - EE 205 - Spr 2022
/////
/////
///// @file addCats.c
///// @version 1.0
/////
///// @warning This program must be compiled on the platform it's exploring.
/////
///// @see https://en.wikipedia.org/wiki/Integer_overflow
/////
///// @author Jordan Cortado <jcortado@hawaii.edu>
///// @date   17_Feb_2022
/////////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <stdlib.h>

#include "addCats.h"
#include "catDatabase.h"

int addCat(char name[], int gender, int breed, bool fixed, float weight){

   if ( numberOfCats > MAX_CATS ) {
      printf("No more than %d cats allowed. \n", MAX_CATS);
      return 0;
   }

   if ( strlen( name ) <= 0 ) {
      printf("Cats need to have a name. \n");
      return 0;
   }
  
   if ( strlen( name ) > MAX_NAME ){
      printf("Cats name can not exceed %d characters. \n", MAX_NAME);
      return 0;
   }

   if ( weight <= 0 ){
      printf("Cats need to have a weight. \n");
      return 0;
   }

   for ( int i = 0; i <= numberOfCats; ++i){
      if ( strcmp( name, arrayName[ i ] ) == 0 ){
         printf("Name of each cat must be unique. \n");
         return 0;
      }
   }

   strcpy( arrayName[ numberOfCats ] , name );
   #ifdef DEBUG
   printf("The name of the cat is: %s \n", name);
   printf("The name stored in the array is: %s \n", arrayName[ numberOfCats ]);
   #endif

   arrayGender [ numberOfCats ] = gender;
   #ifdef DEBUG
   printf("The gender of the cat is: %d \n", numberOfCats);
   printf("The gender stored in the array is: %d \n", arrayGender[ numberOfCats ]);
   #endif

   arrayBreed [ numberOfCats ] = breed;
   #ifdef DEBUG
   printf("The breed of the cat is: %d \n", breed);
   printf("The breed stored in the array is: %d \n", arrayBreed[ numberOfCats ]);
   #endif

   arrayFixed [ numberOfCats ] = fixed;
   #ifdef DEBUG
   printf("Is the cat fixed? %d \n", fixed);
   printf("Stored in the array is: %d \n", arrayFixed[ numberOfCats ]);
   #endif

   arrayWeight [ numberOfCats ] = weight;
   #ifdef DEBUG
   printf("The weight of the cat is: %f \n", weight);
   printf("The weight stored in the array is: %f \n", arrayWeight[ numberOfCats ]);
   #endif

   #ifdef DEBUG
   printf("The current number of cats is: %d \n", numberOfCats);
   #endif 


   numberOfCats++;
   
   #ifdef DEBUG 
   printf("This cat has been added. \n\n");
   #endif
   
   return 1;
}
