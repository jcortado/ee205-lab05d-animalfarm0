///////////////////////////////////////////////////////////////////////////////
/////         University of Hawaii, College of Engineering
///// @brief  Lab 05d - catDatabase - EE 205 - Spr 2022
/////
/////
///// @file catDatabase.c
///// @version 1.0
/////
///// @warning This program must be compiled on the platform it's exploring.
/////
///// @see https://en.wikipedia.org/wiki/Integer_overflow
/////
///// @author Jordan Cortado <jcortado@hawaii.edu>
///// @date   17_Feb_2022
/////////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <stdlib.h>

#include "catDatabase.h"

int numberOfCats = 0;

char arrayName[MAX_CATS] [MAX_NAME];

enum GENDER arrayGender[MAX_CATS];

enum BREED arrayBreed[MAX_CATS];

bool  arrayFixed[MAX_CATS];

float arrayWeight[MAX_CATS];

