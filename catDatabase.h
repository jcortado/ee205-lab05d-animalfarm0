///////////////////////////////////////////////////////////////////////////////
/////         University of Hawaii, College of Engineering
///// @brief  Lab 05d - Animal farm 0 - EE 205 - Spr 2022
/////
/////
///// @file catDatabase.h
///// @version 1.0
/////
///// @warning This program must be compiled on the platform it's exploring.
/////
///// @see https://en.wikipedia.org/wiki/Integer_overflow
/////
///// @author Jordan Cortado <jcortado@hawaii.edu>
///// @date   17_Feb_2022
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <stdlib.h>

#define MAX_NAME (30)
#define MAX_CATS (7)

//#define DEBUG

enum GENDER { UNKOWN_GENDER = 0, MALE = 1, FEMALE = 2 };

enum BREED { UNKNOWN_BREED , MAINE_COON, MANX, SHORTHAIR, PERSIAN, SPHYNX };

extern   int     numberOfCats; 

extern   char    arrayName[MAX_CATS] [MAX_NAME];

extern   enum    GENDER arrayGender[MAX_CATS];

extern   enum    BREED arrayBreed[MAX_CATS];

extern   bool    arrayFixed[MAX_CATS];

extern   float   arrayWeight[MAX_CATS];
