///////////////////////////////////////////////////////////////////////////////
/////         University of Hawaii, College of Engineering
///// @brief  Lab 05d - Animal farm 0 - EE 205 - Spr 2022
/////
///// Usage:
/////
///// Result:
/////   
/////
///// Example:
///// 
/////  
/////
///// @file deleteCats.c
///// @version 1.0
/////
///// @warning This program must be compiled on the platform it's exploring.
/////
///// @see https://en.wikipedia.org/wiki/Integer_overflow
/////
///// @author Jordan Cortado <jcortado@hawaii.edu>
///// @date   22_Feb_2022
/////////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <stdlib.h>

#include "catDatabase.h"

void deleteAllCats () {

   memset ( arrayName, 0, sizeof arrayName );

   memset ( arrayGender, 0, sizeof arrayGender );

   memset ( arrayBreed, 0, sizeof arrayBreed );

   memset ( arrayFixed, 0, sizeof arrayFixed );

   memset ( arrayWeight, 0, sizeof arrayWeight );

   numberOfCats = 0;

   printf("\nDeleted all cats. Database is empty.\n");

}

