///////////////////////////////////////////////////////////////////////////////
/////         University of Hawaii, College of Engineering
///// @brief  Lab 05d - Animal farm 0 - EE 205 - Spr 2022
/////
/////
///// @file deleteCats.h
///// @version 1.0
/////
///// @warning This program must be compiled on the platform it's exploring.
/////
///// @see https://en.wikipedia.org/wiki/Integer_overflow
/////
///// @author Jordan Cortado <jcortado@hawaii.edu>
///// @date   20_Feb_2022
/////////////////////////////////////////////////////////////////////////////////

#pragma once

#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <stdlib.h>

extern void deleteAllCats();

