///////////////////////////////////////////////////////////////////////////////
/////         University of Hawaii, College of Engineering
///// @brief  Lab 05d - AnimalFarm0 - EE 205 - Spr 2022
/////
/////
///// @file main.c
///// @version 1.0
/////
///// @warning This program must be compiled on the platform it's exploring.
/////
///// @see https://en.wikipedia.org/wiki/Integer_overflow
/////
///// @author Jordan Cortado <jcortado@hawaii.edu>
///// @date   17_Feb_2022
/////////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <stdlib.h>

#include "catDatabase.h"
#include "addCats.h"
#include "reportCats.h"
#include "updateCats.h"
#include "deleteCats.h"

//#define TESTER


int main ( ) {
   
   printf("\nStarting Animal Farm 0\n\n");
   
#ifdef TESTER
   
   //             addCats
   addCat( "Loki",   MALE,          PERSIAN,    true,     8.5 );
   addCat( "Milo",   MALE,          MANX,       true,     7.0 );
   addCat( "Bella",  FEMALE,        MAINE_COON, true,    18.2 );
   addCat( "Kali",   FEMALE,        SHORTHAIR,  false,    9.2 );
   addCat( "Trin",   FEMALE,        MANX,       true,    12.2 );
   addCat( "Chili",  UNKOWN_GENDER, SHORTHAIR,  true,     7.0 );
   addCat( "Mango",  MALE,          UNKNOWN_BREED,  false, 15.4 );  //Added my own cat. Others are from the example 
  

   //             reportCats
   printAllCats();                  //print all added cats

   printCat ( 6 );                  //Should print Mango credentials

   findCat ( "Mango" );             //Where's Mango?
   

   //             updateCats
   updateCatName ( 0, "Thor" );     //Loki is now Thor

   fixCat ( 0 );                    //Is Thor fixed?

   printCat ( 0 );                  //Print Thors credentials


   //             deleteCats
   deleteAllCats();                  //Deletes all cats in database

   printAllCats();                  //Nothing should print. All cats deleted.

#endif

   addCat( "Loki",   MALE,             PERSIAN,       true,     8.5 );
   addCat( "Milo",   MALE,             MANX,          true,     7.0 );
   addCat( "Bella",  FEMALE,           MAINE_COON,    true,    18.2 );
   addCat( "Kali",   FEMALE,           SHORTHAIR,     false,    9.2 );
   addCat( "Trin",   FEMALE,           MANX,          true,    12.2 );
   addCat( "Chili",  UNKOWN_GENDER,    SHORTHAIR,     true,     7.0 );

   printAllCats();

   int kali = findCat ( "Kali" );
   updateCatName ( kali, "Chili" );
   printCat ( kali );
   updateCatName ( kali, "Capulet" );
   updateCatWeight ( kali, 9.9 ) ;
   fixCat ( kali );
   printCat ( kali );

   printAllCats();

   deleteAllCats();
   printAllCats();


   printf("Done with Animal Farm 0\n\n");

   return 0;
}
