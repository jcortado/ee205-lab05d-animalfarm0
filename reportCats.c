///////////////////////////////////////////////////////////////////////////////
/////         University of Hawaii, College of Engineering
///// @brief  Lab 05d - Animal farm 0 - EE 205 - Spr 2022
/////
/////
///// @file reportCats.c
///// @version 1.0
/////
///// @warning This program must be compiled on the platform it's exploring.
/////
///// @see https://en.wikipedia.org/wiki/Integer_overflow
/////
///// @author Jordan Cortado <jcortado@hawaii.edu>
///// @date   17_Feb_2022
/////////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <stdlib.h>

#include "catDatabase.h"

void printCat ( int index ) {

   if ( index < 0 || index > MAX_CATS ) {
      printf("animalfarm0: Bad cat %d \n", index);
   }
   else {
      printf("cat index = %d \n", index);
      printf("name = %s \n", arrayName[index]);
      printf("gender = %d \n", arrayGender[index]);
      printf("breed = %d \n", arrayBreed[index]);
      printf("isFixed = %d \n", arrayFixed[index]);
      printf("weight = %f \n\n", arrayWeight[index]);
   }

}


void printAllCats() {

   if ( numberOfCats == 0 ){
      printf("No cats in the database.\n");
   }

   for ( int index = 0; index < numberOfCats; ++index){
      printf("cat index = %d \n", index);
      printf("name = %s \n", arrayName[index]);
      printf("gender = %d \n", arrayGender[index]);
      printf("breed = %d \n", arrayBreed[index]);
      printf("isFixed = %d \n", arrayFixed[index]);
      printf("weight = %f \n\n", arrayWeight[index]);
   }

}


int findCat( char name[] ){

   for ( int index = 0; index < MAX_CATS; ++index ){

      if ( strcmp( arrayName[index], name ) == 0 ) {
         printf("%s is cat number [%d]\n\n", name, index);
      }
   }

   return 0;
}


