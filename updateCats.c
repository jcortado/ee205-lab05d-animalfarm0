///////////////////////////////////////////////////////////////////////////////
/////         University of Hawaii, College of Engineering
///// @brief  Lab 05d - Animal farm 0 - EE 205 - Spr 2022
/////
/////
///// @file updateCats.c
///// @version 1.0
/////
///// @warning This program must be compiled on the platform it's exploring.
/////
///// @see https://en.wikipedia.org/wiki/Integer_overflow
/////
///// @author Jordan Cortado <jcortado@hawaii.edu>
///// @date   21_Feb_2022
/////////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <stdlib.h>

#include "catDatabase.h"

void updateCatName ( int index, char newName[] ) {

   if ( index < 0 ) {
      printf("Index cannot be negative.\n");
   }

   if ( index >= numberOfCats ) {
      printf("There is no cat in index [%d] \n", index);
   }

   if ( strlen(newName) == 0 ) {
      printf("Cat needs to have a name. \n");
   }

   if ( strlen(newName) > MAX_NAME ) {
      printf("Cat name cannot exceed %d characters. \n", MAX_NAME);
   }

   
   for ( int i = 0; i <= numberOfCats; i++) {
      
      if ( strcmp( newName, arrayName[i]) == 0) {
         printf("Cat new name must be unique.\n");
      }
   }

   
   strcpy( arrayName[index], newName );

   printf("Cat name has been changed to %s. \n", newName);
}


void fixCat( int index ) {

   if ( arrayFixed[index] == 0 ) {
      arrayFixed[index] = true;
      printf("%s is not fixed. \n", arrayName[index]);
   }
   else {
      arrayFixed[index] = false;
      printf("%s is fixed. \n", arrayName[index]);
   }
}


void updateCatWeight ( int index, float newWeight) {

   if ( newWeight <= 0 ) {
      printf("Cat weight must be more than 0\n");
   }
   else {
      arrayWeight[index] = newWeight;
      printf("Cat now weighs %f. \n", newWeight);

   }

}
